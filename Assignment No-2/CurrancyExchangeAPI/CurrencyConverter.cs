﻿namespace CurrancyExchangeAPI
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public CurrencyExchangeResponse GetConvertedCurrencies(double exchangeRate, CurrencyExchangeRequest currencyExchangeRequest)
        {

            double convertToCurrency = 0.0;
            var currencyExchangeResponse = new CurrencyExchangeResponse();
            currencyExchangeResponse.Amounts = new List<double>();

            foreach (double amountToConvert in currencyExchangeRequest.Amounts)
            {
                convertToCurrency = (amountToConvert * (double)exchangeRate);
                currencyExchangeResponse.Amounts.Add(convertToCurrency);

            }
            return currencyExchangeResponse;

        }

    }

    public interface ICurrencyConverter
    {
        CurrencyExchangeResponse GetConvertedCurrencies(double exchangeRate, CurrencyExchangeRequest currencyExchangeRequest);
    }


}
