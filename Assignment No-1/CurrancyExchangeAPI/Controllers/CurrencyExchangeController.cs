using Microsoft.AspNetCore.Mvc;
using System.Text.Json;


namespace CurrancyExchangeAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CurrencyExchangeController : ControllerBase
    {
        private readonly ICurrencyConverter _currencyConverter;

        public CurrencyExchangeController(ICurrencyConverter currencyConverter)
        {
            _currencyConverter = currencyConverter;
        }
        [HttpGet]
        public async Task<string> Get()
        {
            var url = $"https://v6.exchangerate-api.com/v6/844bf3399a357fc43ec0f4a7/latest/USD";
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            var rates = await response.Content.ReadAsStringAsync();
            return rates;

        }
        [HttpPost]
        public async Task<ActionResult> GetConvertedAmounts(CurrencyExchangeRequest currencyExchangeRequest)
        {


            var url = $"https://v6.exchangerate-api.com/v6/844bf3399a357fc43ec0f4a7/latest/{currencyExchangeRequest.ConvertFrom}";
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            var currencyResponse = await response.Content.ReadAsStringAsync();
            CurrencyRate currencyRates = null;
            if (currencyResponse == null)
                return NotFound();
            currencyRates = JsonSerializer.Deserialize<CurrencyRate>(currencyResponse);



            if (currencyRates == null || currencyRates.result == "error")
                return NotFound();

            if (currencyRates.conversion_rates.GetType().GetProperty(currencyExchangeRequest.ConvertTo) == null)
            {
                return BadRequest("please specify Right ConvertTo value");
                Environment.Exit(0);

            }

            var rate = currencyRates.conversion_rates.GetType().GetProperty(currencyExchangeRequest.ConvertTo).GetValue(currencyRates.conversion_rates);
            var currencyExchangeLogicResponse = new CurrencyExchangeResponse();

            double ConversionRate;
            Boolean checkConvertion = double.TryParse(rate.ToString(), out ConversionRate);
            if (checkConvertion)
            {
                currencyExchangeLogicResponse = _currencyConverter.GetConvertedCurrencies(ConversionRate, currencyExchangeRequest);

            }
            else
            {
                return BadRequest("Casting fail");
            }

            return Ok(currencyExchangeLogicResponse);



        }
    }
}