﻿namespace CurrencyExchangeAPI
{
    public interface ILogger
    {
        void LogData(Log log);
        List<Log> GetRequestLog();
        
    }
}
