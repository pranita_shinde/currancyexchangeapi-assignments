﻿using CurrencyExchangeAPI.Interface;
using System.Text.Json;


namespace CurrencyExchangeAPI
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public static async Task<ExchangeRate> GetExchangeRate(CurrencyExchange currency)
        {
            var url = $"https://v6.exchangerate-api.com/v6/844bf3399a357fc43ec0f4a7/latest/{currency.ConvertFrom}";
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            var currencyResponse = await response.Content.ReadAsStringAsync();
            ExchangeRate exchangeRate = null;

            exchangeRate = JsonSerializer.Deserialize<ExchangeRate>(currencyResponse);

            return exchangeRate;
        }

        public  async Task<CurrencyExchangeResponse> GetConvertedResult(CurrencyExchangeRequest currencyExchangeRequest)
        {
            //var exchangeRate = await GetExchangeRate(currencyExchangeRequest);
            var tasks = new List<Task<ExchangeRate>>();

            var task1= GetExchangeRate(currencyExchangeRequest.Currencies[0]);
            var task2= GetExchangeRate(currencyExchangeRequest.Currencies[1]);
            var task3= GetExchangeRate(currencyExchangeRequest.Currencies[2]);

            await Task.WhenAll(task1, task2,task3);

            var resultA = task1.Result;
            var resultB = task1.Result;
            var resultC = task1.Result;


            for (int i=0;i< currencyExchangeRequest.Currencies.Count;i++ )
            {
                tasks.Add(GetExchangeRate(currencyExchangeRequest.Currencies[i]));
              //await Task.WhenAll(task1, task2, task3);
            }
            var results = new List<Task>();

            //foreach (var item in tasks)
            //{
            //    results.Add(tasks[item].Result);
            //}
  
            var conversionRateA = resultA.conversion_rates.GetType().GetProperty(currencyExchangeRequest.Currencies[0].ConvertTo).GetValue(resultA.conversion_rates);
            var conversionRateB = resultB.conversion_rates.GetType().GetProperty(currencyExchangeRequest.Currencies[0].ConvertTo).GetValue(resultB.conversion_rates);
            var conversionRateC = resultC.conversion_rates.GetType().GetProperty(currencyExchangeRequest.Currencies[0].ConvertTo).GetValue(resultC.conversion_rates);

            var conversionRatesA = double.Parse(conversionRateA.ToString());
           // var conversionRatesB = double.Parse(conversionRateB.ToString());

           // var conversionRatesC = double.Parse(conversionRateC.ToString());


            var currencyExchangeResponse = new CurrencyExchangeResponse();
            currencyExchangeResponse.Amounts = new List<double>();
            double convertToCurrency = 0.0;
            foreach (var amountToConvert in currencyExchangeRequest.Currencies)
            {
                convertToCurrency = (amountToConvert.Amount * (double)conversionRatesA);
                currencyExchangeResponse.Amounts.Add(convertToCurrency);
            }
            return currencyExchangeResponse;
        }
    }
    
}
