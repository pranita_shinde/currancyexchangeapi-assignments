﻿namespace CurrencyExchangeAPI
{
    public class Log
    {
        
        public string RequestMessage { get; set; }
        public string ResponseMessage { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
