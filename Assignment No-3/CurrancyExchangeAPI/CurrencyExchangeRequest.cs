﻿namespace CurrancyExchangeAPI
{
    public class CurrencyExchangeRequest
    {
        public List<double> Amounts { get; set; }
        public string ConvertTo { get; set; }
        public string ConvertFrom { get; set; }
    }
}
