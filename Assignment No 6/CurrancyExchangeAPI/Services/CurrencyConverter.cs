﻿using CurrencyExchangeAPI.Interface;
using System.Text.Json;


namespace CurrencyExchangeAPI
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public static async Task<ExchangeRate> GetExchangeRate(CurrencyExchange currency)
        {
            try
            {
                var url = $"https://v6.exchangerate-api.com/v6/844bf3399a357fc43ec0f4a7/latest/{currency.ConvertFrom}";
                var httpClient = new HttpClient();
                var response = await httpClient.GetAsync(url);
                var currencyResponse = await response.Content.ReadAsStringAsync();
                ExchangeRate exchangeRate = null;

                exchangeRate = JsonSerializer.Deserialize<ExchangeRate>(currencyResponse);

                return exchangeRate;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<CurrencyExchangeResponse> GetConvertedResult(CurrencyExchangeRequest currencyExchangeRequest)
        {
            try
            {

                var tasks = new List<Task<ExchangeRate>>();

                foreach (var currencies in currencyExchangeRequest.Currencies)
                {
                    var response = GetExchangeRate(currencies);
                    tasks.Add(response);
                }
                var results = await Task.WhenAll(tasks);
                Console.WriteLine(results.ToString());
                CurrencyExchangeResponse currencyExchangeResponse = new CurrencyExchangeResponse();

                List<double> rates = new List<double>();
                if (results != null)
                {
                    for (int i = 0; i < results.Length; i++)
                    {
                        var conversionRate = results[i].conversion_rates.GetType().GetProperty(currencyExchangeRequest.Currencies[i].ConvertTo).GetValue(results[i].conversion_rates);
                        rates.Add(double.Parse(Convert.ToString(conversionRate)));

                    }
                }
                currencyExchangeResponse.Results = new List<double>();

                var counter = 0;
                foreach (var currencies in currencyExchangeRequest.Currencies)
                {
                    var convertToCurrency = 0.0;
                    convertToCurrency = (currencies.Amount * rates[counter]);
                    counter++;

                    currencyExchangeResponse.Results.Add(convertToCurrency);
                }
                return currencyExchangeResponse;
            }

            catch (Exception ex)
            {
                throw null;
            }
        }
    }

}
