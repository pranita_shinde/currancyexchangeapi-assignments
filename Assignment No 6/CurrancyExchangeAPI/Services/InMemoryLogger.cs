﻿namespace CurrencyExchangeAPI
{
    public class InMemoryLogger : ILogger
    {
        private static List<Log> Logs = new List<Log>();
        public List<Log> GetRequestLog()
        {
            return Logs;
        }
        public void LogData(Log log)
        {
            Logs.Add(log);
        }
    }
}
