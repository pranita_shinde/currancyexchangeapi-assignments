﻿namespace CurrencyExchangeAPI
{
    public interface ILogger
    {
        public void LogData(Log log);
        List<Log> GetRequestLog();
        
    }
}
