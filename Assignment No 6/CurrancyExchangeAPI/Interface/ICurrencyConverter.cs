﻿namespace CurrencyExchangeAPI.Interface
{
    public interface ICurrencyConverter
    {
        Task<CurrencyExchangeResponse> GetConvertedResult(CurrencyExchangeRequest currencyExchangeRequest);

    }
}
