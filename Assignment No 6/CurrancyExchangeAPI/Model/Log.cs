﻿using System;
using System.Collections.Generic;

namespace CurrencyExchangeAPI
{
    public partial class Log
    {
        public int Id { get; set; }
        public string? RequestMessage { get; set; }
        public string? ResponseMessage { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
