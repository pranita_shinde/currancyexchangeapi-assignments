﻿namespace CurrencyExchangeAPI
{
    public class CurrencyExchangeRequest
    {
        public List<CurrencyExchange> Currencies { get; set; }
        
    }

    public class CurrencyExchange
    {
        public double Amount { get; set; }
        public string ConvertTo { get; set; }
        public string ConvertFrom { get; set; }
    }
}
