using CurrencyExchangeAPI;
using CurrencyExchangeAPI.DataLayer;
using CurrencyExchangeAPI.Interface;
using CurrencyExchangeAPI.Services;


var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<LoggingDatabaseSettings>(builder.Configuration.GetSection("LoggingDatabaseSettings"));

// Add services to the container.
builder.Services.AddTransient<ICurrencyConverter, CurrencyConverter>();
//builder.Services.AddTransient<CurrencyExchangeAPI.ILogger, FileLogger>();
//builder.Services.AddTransient<CurrencyExchangeAPI.ILogger, DatabaseLogger>();
builder.Services.AddSingleton<CurrencyExchangeAPI.ILogger, MongoDBLogger>();
//builder.Services.AddSingleton<CurrencyExchangeAPI.ILogger,InMemoryLogger>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
