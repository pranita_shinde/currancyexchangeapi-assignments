﻿using Microsoft.AspNetCore.Mvc;

namespace CurrencyExchangeAPI
{
    public class CurrencyExchangeResponse
    {
        public List<double> Amounts { get; set; }

        public static explicit operator ObjectResult(CurrencyExchangeResponse v)
        {
            throw new NotImplementedException();
        }
    }
}
