﻿using MongoDB.Bson.Serialization.Attributes;

namespace CurrencyExchangeAPI
{
    public partial class Log
    { 
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public String? Id { get; set; }
        public string? RequestMessage { get; set; }
        public string? ResponseMessage { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
