﻿namespace CurrencyExchangeAPI.DataLayer
{
    public class LoggingDatabaseSettings
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;

        public string CollectionName { get; set; } = null!;
    }
}
