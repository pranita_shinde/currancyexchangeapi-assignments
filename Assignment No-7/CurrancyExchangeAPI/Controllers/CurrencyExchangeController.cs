using CurrencyExchangeAPI.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace CurrencyExchangeAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CurrencyExchangeController : ControllerBase
    {
        private readonly ICurrencyConverter _currencyConverter;
        private readonly CurrencyExchangeAPI.ILogger _logger;

        public CurrencyExchangeController(ICurrencyConverter currencyConverter, CurrencyExchangeAPI.ILogger logger)
        {
            _currencyConverter = currencyConverter;
            _logger = logger;

        }
        [HttpPost]
        public async Task<IActionResult> GetConvertedAmounts(CurrencyExchangeRequest currencyExchangeRequest)
        {
            
            string requestString = JsonSerializer.Serialize<CurrencyExchangeRequest>(currencyExchangeRequest);
            var currencyConverter = new CurrencyConverter();

            var result = await currencyConverter.GetConvertedResult(currencyExchangeRequest);
            if (result == null)
            {
                return NotFound();
            }
            #region Logging
            string responseString = JsonSerializer.Serialize<CurrencyExchangeResponse>(result);
            _logger.LogData(new Log
            {
                RequestMessage = requestString,
                ResponseMessage = responseString,
                CreatedOn = DateTime.Now      
            });
            #endregion
            
            return Ok(result);
        }
        [HttpGet]
        public async Task<IActionResult> GetLog()
        {
             var result =  _logger.GetRequestLog();
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
    }
}
