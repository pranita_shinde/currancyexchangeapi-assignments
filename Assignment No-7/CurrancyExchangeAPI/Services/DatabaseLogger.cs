﻿using System.Linq;
using System.Text.Json;

namespace CurrencyExchangeAPI.Services
{
    public class DatabaseLogger : ILogger
    {
        public List<Log> GetRequestLog()
        {
            List<Log> logs;
            using (var context = new LoggingContext())
            {
                logs = context.Logs.OrderByDescending(p => p.CreatedOn)
                    .Take(5).ToList<Log>();
            }
            return logs;
        }
        public void LogData(Log log)
        {
            using (var _context = new LoggingContext())
            {
                _context.Logs.Add(log);
                _context.SaveChanges();
            }

        }

    }
}

