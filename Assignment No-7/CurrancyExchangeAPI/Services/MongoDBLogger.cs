﻿using CurrencyExchangeAPI.DataLayer;
using CurrencyExchangeAPI.Provider;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace CurrencyExchangeAPI.Services
{
    public class MongoDBLogger : IMongoProvider
    {
        private readonly IMongoCollection<Log> _logCollection;

        public MongoDBLogger(IOptions<LoggingDatabaseSettings> options)
        {
            if (options.Value.ConnectionString != null)
            {
                var mongoClient = new MongoClient(options.Value.ConnectionString);

                _logCollection = mongoClient.GetDatabase(options.Value.DatabaseName)
                                .GetCollection<Log>(options.Value.CollectionName);
            }
        }

        public List<Log> GetRequestLog()
        {
            return _logCollection.Find(x => true).SortByDescending(e => e.CreatedOn).Limit(2).ToList();
        }
       
        public async void LogData(Log log)
        {
            await _logCollection.InsertOneAsync(log);
        }
    }
}
