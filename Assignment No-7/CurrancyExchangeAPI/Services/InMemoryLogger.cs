﻿using CurrencyExchangeAPI.Provider;

namespace CurrencyExchangeAPI
{
    public class InMemoryLogger : ISQLProvider
    {
        private static List<Log> Logs = new List<Log>();
        public List<Log> GetRequestLog()
        {
            return Logs;
        }
        public void LogData(Log log)
        {
            Logs.Add(log);
        }
    }
}
