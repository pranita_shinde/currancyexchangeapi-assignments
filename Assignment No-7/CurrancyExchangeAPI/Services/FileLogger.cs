﻿
using System.Text.Json;

namespace CurrencyExchangeAPI
{
    public class FileLogger : ILogger
    {
        private static List<Log> Logs = new List<Log>();

        public List<Log> GetRequestLog()
        {
            return Logs;
        }
        public void LogData(Log log)
        {
            string requestString = JsonSerializer.Serialize<Log>(log);

            using (StreamWriter file = new StreamWriter("C:/Users/abc/Desktop/CurrancyExchangeAPI/CurrancyExchangeAPI/CurrancyExchangeAPI/log/log.txt", true))
            {
                file.WriteLine($"{DateTime.Now} : { requestString}");
            }
            
        }
    }
}
