﻿//using CurrencyExchangeAPI.Controllers;
//using CurrencyExchangeAPI.Interface;
//using Moq;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Xunit;

//namespace CurrencyExchangeAPI.Test.Controllers
//{
//    public class CurrencyExchangeControllerTests
//    {
//        private MockRepository mockRepository;

//        public CurrencyExchangeControllerTests()
//        {
//            this.mockRepository = new MockRepository(MockBehavior.Strict);

//        }

//        [Fact]
//        public async Task GetConversion_ValidInput_Returns200()
//        {
//            // Arrange
//            var converter = new Mock<ICurrencyConverter>();
//            var converter2 = new Mock<ILogger>();


//            // Arrange
//            var currencyExchangeController = new CurrencyExchangeController(converter.Object, converter2.Object);
//            var amounts = new List<double>();
//            amounts.Add(1);
//            amounts.Add(1);
//            amounts.Add(1);
//            var currencyExchangeRequest = new CurrencyExchangeRequest
//            {
//                //Amounts = amounts,
//                //ConvertTo = "INR",
//                //ConvertFrom = "USD"

//            };

//            // Act
//            var result = await currencyExchangeController.GetConvertedAmounts(currencyExchangeRequest);
//            var objectResult = (Microsoft.AspNetCore.Mvc.ObjectResult)result;

//            // Assert
//            Assert.True(objectResult.StatusCode == 200);
//        }

//        [Fact]
//        public async Task GetConversion_InvalidInput_Return404()
//        {
//            var converter = new Mock<ICurrencyConverter>();
//            // Arrange
//            var currencyExchangeController = new CurrencyExchangeController(converter.Object);
//            var amounts = new List<double>();
//            amounts.Add(1);
//            amounts.Add(1);
//            amounts.Add(1);
//            var currencyExchangeRequest = new CurrencyExchangeRequest
//            {
//                Amounts = amounts,
//                ConvertTo = "INR",
//                ConvertFrom = "www"

//            };

//            // Act
//            var result =await currencyExchangeController.GetConvertedAmounts(currencyExchangeRequest);
//           var objectResult = (Microsoft.AspNetCore.Mvc.NotFoundResult)result;

//            // Assert;
//           Assert.True(objectResult.StatusCode == 404);
//        }


//    }
//}
